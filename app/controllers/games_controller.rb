class GamesController < ApplicationController

  def index
    @games = Game.where(:ranking.ne => 0).order_by(:ranking => :asc).limit(300).all
  end

end
