class Game

  include NoBrainer::Document

  field :name, validates: { presence: true }
  field :bgg_id, type: Numeric
  field :ranking, type: Numeric
  field :abstract_ranking, type: Numeric
  field :childrens_ranking, type: Numeric
  field :customizable_ranking, type: Numeric
  field :family_ranking, type: Numeric
  field :party_ranking, type: Numeric
  field :strategy_ranking, type: Numeric
  field :thematic_ranking, type: Numeric
  field :war_ranking, type: Numeric
  field :rating, type: Float
  field :image_url
  field :url
  field :release_date
  field :updated_at, type: DateTime

  def self.update_games
    # BGG::Games.games(:war).each{|bgg| game=Game.where(:bgg_id => bgg.bgg_id).first || Game.new; game.update_attributes(bgg.to_h.reject{|k,v| k==:ranking}.merge(:war_ranking => bgg.ranking)) };
    # Game.each {|g| g.update_attributes(ranking: g.ranking.to_i) }
    # cats = [:abstract, :childrens, :customizable, :family, :party, :strategy, :thematic, :war]
    # cats.each{|c| Game.each {|g| g.update_attributes(:"#{c}_ranking" => g[:"#{c}_ranking"].to_i) };
  end

end
