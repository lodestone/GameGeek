require "test_helper"

class RequirementsTest < ActionDispatch::IntegrationTest

  describe "The Litmus Test App" do

    setup do
      CurrentStatus.destroy_all
      @status = CurrentStatus.new status: "UP", message: "All systems normal"
      @status.save
    end

    it "should have a main page" do
      get "/"
      assert_response 200
    end

    describe "should have a single page showing the current status" do
      setup do
        get "/"
      end

      it "should show the status [UP or DOWN]" do
        assert_match "UP", response.body
        assert_equal "UP", assigns(:current_status).status
      end

      it "should show the check time" do
        assert_match @status.checktime.to_s(:db), response.body
        assert_equal @status.checktime.to_s(:db), assigns(:current_status).checktime.to_s(:db)
      end

      it "should show the message" do
        assert_match @status.message, response.body
        assert_equal @status.message, assigns(:current_status).message
      end
    end

    it "should show the history of the last 10 status messages with their times" do
      10.times { CurrentStatus.new(status: "DOWN", message: "Broken!").save }
      get "/"
      assert_select "#previous-statuses .previous-status" do |sel|
        assert_equal 10, sel.count
      end
    end

    it "should support ONLY 2 possible status options: UP or DOWN" do
      post "/status", {status: "NOT_VALID"}
      assert_match "status should be UP or DOWN", response.body
    end

    it "should allow updating of a status message without changing the status" do
      put "/status/current", {status: "DOWN"}
      get "/"
      assert_select "#current-status", "DOWN"
    end

    it "should allow changing of the status without leaving a status message" do
      assert_no_difference 'CurrentStatus.count' do
        get "/"
        assert_select "#current-message", "All systems normal"

        # ----
        put "/status/current", {message: "Changing message"}
        # ----

        get "/"
        assert_select "#current-message", "Changing message"
      end
    end

  end

end
