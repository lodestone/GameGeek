# Configuration for RethinkDB and NobRainer

rdb_url       = "localhost"
rethinkdb_url = "rethinkdb://#{rdb_url}/#{Rails.application.class.to_s.gsub(/::.*/, '').underscore}_#{Rails.env}"

NoBrainer.configure do |config|
  config.rethinkdb_url          = rethinkdb_url
  config.logger                 = Rails.logger || Logger.new(STDERR)
  config.colorize_logger        = true
  config.warn_on_active_record  = true
  config.auto_create_databases  = true
  config.auto_create_tables     = true
  config.max_reconnection_tries = 10
  config.durability             = Rails.env.development? || Rails.env.test? ? :soft : :hard
end


include RethinkDB::Shortcuts
conn = r.connect(:host => rdb_url, :port=>28015).repl
conn.use("#{Rails.application.class.to_s.gsub(/::.*/, '').underscore}_#{Rails.env}")






